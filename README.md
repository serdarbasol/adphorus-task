#### This assignment created with create-react-app.
------

Install dependencies with `npm install` or `yarn`.

Start project with `npm start` or `yarn start` on `localhost:3000`.

Run unit test with `npm run test` or `yarn test`.

You can see test coverage, you should run `npm run coverage` or `yarn coverage`.


#### Demo
------
[Try demo](https://adphorus-react-task.herokuapp.com).

#### You can see sketch design in `src/sketch_design`
------
![Screenshot sketch design](https://bitbucket.org/serdarbasol/adphorus-task/raw/29d2a9c542cbb7f7025b3e2a1c2e18429a5c913b/src/sketch_design/screen_shot.png)