import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getList } from '../actions/contactList';
import RecursiveContactList from '../components/recursiveContactList';

class ContactList extends Component {
	componentDidMount() {
		this.props.getList();
	}

  render() {
		const { isFetching, list } = this.props;
    return (
			<React.Fragment>
				{isFetching ?
          <h1 data-text="LOADING..." className="loading">LOADING...</h1>
          :
          list && list.length > 0 ?
            <div className="Recursive__accordion">
              {list.map((item)=> {
                return (
                  <RecursiveContactList data={item} key={item.ID}/>
                )
              })}
            </div>
          :
          <h3>Data is empty</h3>
				}
			</React.Fragment>
		);
  }
}

ContactList.propTypes = {
  isFetching: PropTypes.bool,
  list: PropTypes.array,
  error: PropTypes.string,
};

const mapStateToProps = state => ({
  isFetching: state.contacts.isFetching,
  list: state.contacts.list,
  error: state.contacts.error,
});

const mapDispatchToProps = dispatch => bindActionCreators({ getList }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);
