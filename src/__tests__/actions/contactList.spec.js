import request from '../../api/request';
const JSON_API = "burakcan/ca77e8fc11a1455cc1962ad7318b8fbc/raw/b446a09888882df7273f17d2ff7ebf4820de4152/dataset.json";
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
const middleware = [thunk];
const mockStore = configureStore(middleware);
function success() {
  return {
    type: 'FETCH_DATA_SUCCESS'
  }
}

function fetchData () {
  return dispatch => {
    return request(`/${JSON_API}`)
      .then(() => dispatch(success()))
  };
}

it('should execute fetch data', () => {
  const store = mockStore({});
  return store.dispatch(fetchData())
    .then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual(success());
    })
});