import reducer from '../../reducers/contactList';
import unFlatten from '../../transformers/unFlatten';
import mockData from '../../__mocks__/mockData';
import tobeMockData from '../../__mocks__/tobeMockData';
import * as CONTACT from '../../constants';
// REQUEST_LIST
// RECEIVE_LIST
// FAILED_LIST

describe('contactList reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        list: [],
        error: null,
      }
    )
  });

  it('should handle CONTACT.REQUEST_LIST', () => {
    expect(
      reducer({
          list: [],
          error: null,
        },{
        type: CONTACT.REQUEST_LIST,
        isFetching: true
      })
    ).toEqual(
      {
        list: [],
        error: null,
        isFetching: true
      }
    )
  });

  it('should handle CONTACT.RECEIVE_LIST', () => {
    expect(
      reducer({
          list: [],
          error: null,
        }, {
        type: CONTACT.RECEIVE_LIST,
        isFetching: false,
        list: unFlatten(mockData)
      })
    ).toEqual(
      {
        list: tobeMockData,
        isFetching: false,
        error: null,
      }
    )
  });

});
    // ,
    // ).toEqual(tobeMockData);
