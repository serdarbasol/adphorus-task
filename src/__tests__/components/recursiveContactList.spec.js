import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import unFlatten from '../../transformers/unFlatten';
import Data from '../../__mocks__/data.json';

import RecursiveContactList from '../../components/RecursiveContactList';

describe('<RecursiveContactList />', () => {
  let data = unFlatten(Data);
  it('checks returned unFlatten data in RecursiveContactList component', () => {
    const wrapper = shallow(<RecursiveContactList data={data} />);
  
    setTimeout(function(){
      wrapper.update();
      expect(wrapper.find('.Recursive__accordion--item')).to.have.lengthOf(data.length);
      done();
    })

  });
});