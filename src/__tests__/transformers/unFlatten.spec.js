import unFlatten from '../../transformers/unFlatten';
import mockData from '../../__mocks__/mockData.json';
import tobeMockData from '../../__mocks__/tobeMockData.json';

test('unFlatten function wrong parameter', () => {
  expect(unFlatten({})).toEqual([]);
});

test('function of returned data should be this mock', () => {
  expect(unFlatten(mockData)).toEqual(tobeMockData);
});