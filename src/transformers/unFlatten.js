/**
 * unFlatten recursive function for tree
 *
 * @export
 * @param {Array} arr
 * @param {Object} parent
 * @param {Array} tree
 * @returns {Array || {message: string}} tree
 */

const unFlatten = (arr, parent = { ID: 0}, tree = []) => {
  if (!Array.isArray(arr)) return [];

  const children = arr.filter((child) => {
    if (!child.parentID) child.parentID = 0;
    return child.parentID === parent.ID
  });
  if (children) {
    parent.ID === 0 ? tree = children : parent.children = children;
    children.map((child) => unFlatten(arr, child));
  }
  return tree;
};

export default unFlatten;

//  const unFlatten = (arr) => {
//   // if (!Array.isArray(arr)) return [];
//
// 	let hashMap  = {},
// 			dataTree = [];
//
//   arr.forEach(data => {
// 		hashMap[data['ID']] = {...data, children: []};
//     if (data['parentID']) hashMap[data['parentID']].children.push(hashMap[data['ID']]);
//     else dataTree.push(hashMap[data['ID']]);
//   });
//
//   return dataTree;
// };