import React, { Component } from 'react';
import deleteIcon from '../assets/images/delete.svg';
import pinIcon from '../assets/images/pin.svg';
import phoneIcon from '../assets/images/phone.svg';

class RecursiveAccordion extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isToggled: false,
			show: true
		};
	};

  collapseItem() {
    this.setState({ isToggled: !this.state.isToggled });
	}

	hideItem() {
    this.setState({ show: false });
	}

	render() {
		return (
			<React.Fragment>
				{this.state.show &&
					<div className="Recursive__accordion--item">
						<div className="Recursive__accordion--item-header"
								 onClick={(e) => this.collapseItem()}>
							<div className={['arrow', this.state.isToggled && 'bottom'].join(' ')}/>
							<h5 className="Info__text--large">{this.props.data.Name}</h5>
							<a className="delete-button" onClick={(e) => {
								e.stopPropagation();
								this.hideItem()
							}}>
								<img src={deleteIcon} alt="delete"/>
							</a>
						</div>
						<div className={['Recursive__accordion--item-body', this.state.isToggled && 'show'].join(' ')}>
							<div className="Recursive__accordion--item-inner">
								<div className="Recursive__accordion--item-info">
									<p className="Info__text Info__text--medium">
										<img src={pinIcon} className="Info__text--icon" alt=""/>
										{this.props.data.City}
									</p>
									<p className="Info__text Info__text--small">
										<img src={phoneIcon} className="Info__text--icon" alt=""/>
										{this.props.data.Phone}
									</p>
								</div>
							</div>
							<div className="Recursive__accordion--children"
									 style={{marginLeft: this.state.isToggled && '35px'}}>
								<span className="vertical-line"/>
								{this.props.data.children && this.props.data.children.map(item => {
									return (
										<RecursiveAccordion data={item} key={item.ID}/>
									)
								})}
							</div>
						</div>
					</div>
				}
			</React.Fragment>
		)
	}

}

export default RecursiveAccordion;