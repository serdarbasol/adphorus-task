import request from '../api/request';
import * as CONTACT from '../constants';
import unFlatten from '../transformers/unFlatten';
const JSON_API = "burakcan/ca77e8fc11a1455cc1962ad7318b8fbc/raw/b446a09888882df7273f17d2ff7ebf4820de4152/dataset.json";
// REQUEST_LIST
// RECEIVE_LIST
// FAILED_LIST

export const getList = () => {
  return (dispatch) => {
  	  dispatch({ type: CONTACT.REQUEST_LIST });
    return request(`/${JSON_API}`)
      .then(response => {
				setTimeout(() => {
          dispatch({
            type: CONTACT.RECEIVE_LIST,
            list: unFlatten(response)
          });
        }, 2000);
      })
      .catch(error => {
      	dispatch({ type: CONTACT.FAILED_LIST, error });
        throw(error);
      });
  };
};