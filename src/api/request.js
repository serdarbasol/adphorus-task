import axios from 'axios';
const URL = "https://gist.githubusercontent.com/";

const client = axios.create({
  baseURL: URL
});

const request = (options) => {
  function onSuccess(response) {
    return response.data;
  }

  const onError = (error) => {

    if (error.response) {
      console.error('Status:',  error.response.status);
      console.error('Data:',    error.response.data);
      console.error('Headers:', error.response.headers);

    } else {
      console.error('Error Message:', error.message);
    }

    return error.response || error.message;
  };

  return client(options)
    .then(onSuccess)
    .catch(onError);
};

export default request;