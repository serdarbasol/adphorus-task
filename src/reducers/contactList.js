import * as CONTACT from '../constants';
// REQUEST_LIST
// RECEIVE_LIST
// FAILED_LIST

const initialState = {
  list: [],
  error: null,
};

export default function contacts(state = initialState, action) {
  switch (action.type) {
    case CONTACT.REQUEST_LIST:
      return {
        ...state,
        isFetching: true
      };

    case CONTACT.RECEIVE_LIST: {
      return {
        ...state,
        list: action.list,
        isFetching: false
      };
    }
    case CONTACT.FAILED_LIST:
      return {
        ...state,
        error: action.error,
        isFetching: false
      };

    default:
      return state
  }
}