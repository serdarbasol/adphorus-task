// import reducer here.
import contacts from './contactList';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  contacts
});

export default rootReducer;
